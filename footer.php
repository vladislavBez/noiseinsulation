<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NoiseInsulation
 */

?>

	</div><!-- #content -->

    <footer id="colophon" class="site-footer">
        <div class="container-content content-background-dark-blue">
            <div class="container container-footer-feedback">
                <div class="row">
                    <div class="col-sm-12 col-md-12 margin-middle-bottom">
                        <h2 class="content-header-middle text-white">Не нашли что искали?</h2> 
                        <h2 class="content-header-small text-white">Оставьте заявку на консультацию</h2>
                        <? echo do_shortcode('[contact-form-7 id="140" title="Заявка на консультацию"]'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-content content-background-dark-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="footer-menu-title">
                            <a href="<?php echo get_home_url(); ?>" class="footer-link">Главная</a>
                        </div>
                        <ul class="footer-menu-list">
                            <li><a class="footer-link" href="<?php echo get_permalink(119); ?>">Скидки</a></li>
                            <li><a class="footer-link" href="<?php echo get_permalink(117); ?>">Установка</a></li>
                            <li><a class="footer-link" href="<?php echo get_permalink(172); ?>">Наш блог</a></li>
                            <li><a class="footer-link" href="<?php echo get_permalink(9); ?>">Аккаунт</a></li>
                            <li><a class="footer-link" href="<?php echo get_permalink(115); ?>">Контакты</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="footer-menu-title">
                            <a class="footer-link" href="<?php echo get_permalink(6); ?>">Каталог</a>
                        </div>
                        <ul class="footer-menu-list">
                            <li><a class="footer-link" href="<?php echo get_category_link(16); ?>">Виброизоляция</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(17); ?>">Шумоподавление</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(18); ?>">Теплоизоляция</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(19); ?>">Декоративные материалы</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(20); ?>">Инструмент</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="footer-menu-title">
                            <a class="footer-link" href="<?php echo get_category_link(21); ?>">Комплекты</a>
                        </div>
                        <ul class="footer-menu-list">
                            <li><a class="footer-link" href="<?php echo get_category_link(40); ?>">Арки</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(35); ?>">Двери</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(36); ?>">Пол</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(37); ?>">Багажник</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(38); ?>">Крыша</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(39); ?>">Капот</a></li>
                            <li><a class="footer-link" href="<?php echo get_category_link(41); ?>">Ванны</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="footer-menu-title">
                            <a class="footer-link" href="<?php echo get_permalink(115); ?>">Контакты</a>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-map-marker"></span>
                            </div>
                            <div class="footer-contact-text">
                                <span>Ярославль, пр-кт Фрунзе, дом 3, офис 312</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-earphone"></span>
                            </div>
                            <div class="footer-contact-text">
                                <span>8(961)021-23-14</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                            <div class="footer-contact-text">
                                <span>info@accord76.shop</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </div>
                            <div class="footer-contact-text footer-contact-column">
                                <div>График работы:</div>
                                <div>Пн - Вт: 10:00 до 20:00</div>
                                <div>Сб: 10:00 до 15:00</div>
                                <div>Вс: выходной</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-content content-background-darkness">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="container-copyright">Copyright © 2018-2018 Все Права Защищены</div>
                    </div>
                </div>
            </div>
        </div>

    </footer><!-- #colophon -->

</div><!-- #page -->

<div id="back-call-paper-modal" class="container-paper-modal hide">
    <div class="body-paper-modal">
        <div class="top-paper-modal">
            <div class="close-paper-modal header-mobile-icon header-icon">
                <span class="glyphicon glyphicon-remove"></span>
            </div>
        </div>
        <div class="content-paper-modal">
            <? echo do_shortcode('[contact-form-7 id="246" title="Заявка на обратный звонок"]'); ?>
        </div>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
