$(document).ready(function () {
    
    $("#main-slider").slick({
        prevArrow: "<div class='slick-arrow slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-arrow slick-next-arrow'></div>"
    });

    $("#goods-in-sets").slick({
        prevArrow: "<div class='slick-arrow slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-arrow slick-next-arrow'></div>"
    });

    $("#goods-in-actions").slick({
        prevArrow: "<div class='slick-arrow slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-arrow slick-next-arrow'></div>"
    });
    
});