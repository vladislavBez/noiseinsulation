$(document).ready(function () {

    if (window.innerWidth >= 992) {

        $(window).bind('scroll', function (e) {
            parallaxScroll();
        });

        function parallaxScroll() {

            //Page height with scrolling
            var clientHeight = document.documentElement.clientHeight;
            //Page scrolling
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop;

            if (scrollTop > clientHeight) {
                $('#container-request-call-parallax').css('z-index', 15);
            } else {
                $('#container-request-call-parallax').css('z-index', 5);
            }

            var scrolled = $(window).scrollTop();
            $('#container-sliders-parallax').css('top', (122 - (scrolled * .25)) + 'px');
            $('#container-request-call-parallax').css('top', (500 - (scrolled * .25)) + 'px');
        }
    }


});