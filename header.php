<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NoiseInsulation
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="icon" href="favicon.png" type="image/png">
	<?php wp_head(); ?>
</head>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49717513 = new Ya.Metrika2({
                    id:49717513,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49717513" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<body id="main-body" <?php body_class(); ?>>
<div id="page" class="site">        
        
    <header id="masthead" class="site-header">

        <div class="container-mobile-header hidden-md hidden-lg">
            <div id="mobile-icon-menu" class="header-icon header-mobile-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24" enable-background="new 0 0 24 24">
                    <g>
                        <path d="M24,3c0-0.6-0.4-1-1-1H1C0.4,2,0,2.4,0,3v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V3z"></path>
                        <path d="M24,11c0-0.6-0.4-1-1-1H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V11z"></path>
                        <path d="M24,19c0-0.6-0.4-1-1-1H1c-0.6,0-1,0.4-1,1v2c0,0.6,0.4,1,1,1h22c0.6,0,1-0.4,1-1V19z"></path>
                    </g>
                </svg>
            </div>
            <div class="header-mobile-logo">
                <a href="<?php echo get_home_url(); ?>" class="header-logo-link">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo.jpg'?>" alt="text">
                </a>
            </div>
            <div class="header-icon header-mobile-icon">
                <a href="tel:+79610212314" class="reset-color">
                    <span class="glyphicon glyphicon-earphone"></span>
                </a>
            </div>
            
            <!--header tablet menu-->
            <div id="header-menu-widget" class="header-menu-widget-container">
                <div class="header-menu-widget">
                    <div class="header-tablet-menu-cart">
                        <div class="tablet-menu-cart">
                            <div class="tablet-menu-cart-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/shopping_cart.png'?>" alt="text">
                            </div>
                            <div class="tablet-menu-cart-info">
                                <div class="tablet-menu-cart-link">
                                    <a href="<?php echo get_page_link(7); ?>">Посмотреть корзину</a>
                                </div>
                                <div class="tablet-menu-cart-text">
                                    <div class="">
                                        <span id="tablet-cart-count">
                                        <?php
                                            echo WC()->cart->get_cart_contents_count();
                                        ?>
                                        </span>
                                        <span> - </span>
                                        <span id="tablet-cart-price">
                                        <?php
                                            echo WC()->cart->get_cart_subtotal();
                                        ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tablet-menu-cart-button">
                            <a class="tablet-menu-cart-checkout" href="<?php echo get_page_link(8); ?>">Оформить заказ</a>
                        </div>
                    </div>
                    <div class="header-tablet-menu-list">
                        <?php wp_nav_menu('menu=MobileMenuFirst'); ?>
                    </div>
                    <div class="header-tablet-menu-categories">
                        <?php echo do_shortcode('[product_categories columns="1" orderby="term_group" parent="0"]'); ?>
                    </div>
                    <div class="header-tablet-menu-categories tablet-children-categories">
                        <?php echo do_shortcode('[product_categories columns="1" orderby="term_group" parent="21"]'); ?>
                    </div>
                    <div class="header-tablet-menu-list">
                        <?php wp_nav_menu('menu=MobileMenuSecond'); ?>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="container-desktop-header hidden-xs hidden-sm">
            <div class="desktop-header-dark">
                <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="container-top-header">
                            <div class="header-search-wrapper">
                                <?php echo do_shortcode('[wcas-search-form]'); ?>
                            </div>
                            <div class="header-info">
                                <div class="contact-header">
                                    <button class="header-contact-button button-open-paper-modal">
                                        <span class="header-contact-glyphicon glyphicon glyphicon-earphone"></span>
                                        <span>Заказать звонок</span>
                                    </button>
                                </div>
                                <div class="header-info-phone">
                                    <span class="header-info-phone-label">Телефон:</span>
                                    <span>8(961)021-23-14</span>
                                </div>
                                <div class="header-info-social">
                                    <div class="header-info-social-icon">
                                        <a href="https://vk.com/accord76shop">
                                            <img src="<?php echo get_template_directory_uri() . '/assets/images/vk-icon.png'?>" alt="text">
                                        </a>
                                    </div>
                                    <div class="header-info-social-icon">
                                        <a href="https://www.facebook.com/accord76.shop/">
                                            <img src="<?php echo get_template_directory_uri() . '/assets/images/facebook-icon.png'?>" alt="text">
                                        </a>
                                    </div>
                                    <div class="header-info-social-icon">
                                        <a href="https://www.instagram.com/accord76.shop/ ">
                                            <img src="<?php echo get_template_directory_uri() . '/assets/images/instagram-icon.png'?>" alt="text">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="desktop-header-white">
                <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 col-md-3">
                        <div class="header-logo">
                            <a href="<?php echo get_home_url(); ?>" class="header-logo-link">
                                <div class="header-logo-img">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo.jpg'?>" alt="text">
                                </div>
                                <div class="header-logo-label">
                                    <div class="header-logo-text">
                                        Шумоизоляция
                                    </div>
                                    <div class="header-logo-text">
                                        для автомобилей
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-6">
                        <div class="header-main-menu">
                            <nav id="site-navigation" class="main-navigation">
                                <?php wp_nav_menu('menu=MainMenu'); ?>
                            </nav><!-- #site-navigation -->
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-3">
                        <!--header cart icon-->
                        <div class="header-icon header-cart">
                            <a href="<?php echo get_page_link(7); ?>">
                                <div class="header-cart-icon">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/shopping_cart.png'?>" alt="text">
                                </div>
                                <?php wp_nav_menu('menu=HeaderCartMenu'); ?>
                            </a>
                        </div>
                        <!--header search icon-->
                        <div id="header-search-icon" class="header-icon header-search">
                            <span class="glyphicon glyphicon-search"></span>
                            <span class="header-search-label">Найти</span>
                        </div>                        
                    </div>
                </div>
                </div>
            </div>
        </div>

        <!--header search widget-->
        <div id="header-search-widget" class="header-search-widget-container">
            <div class="header-search-widget">
                <?php echo do_shortcode('[wcas-search-form]'); ?>
            </div>
        </div>        
        
    </header><!-- #masthead -->
    
    <!--mobile fixed background-->
    <div id="mobile-fixed-background" class="hide"></div>
    
    <div id="content" class="site-content">