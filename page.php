<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NoiseInsulation
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            
            <div class="container-content content-background-lite-gray">
                <div class="container">
                    <div class="row">
                        
                        <?php
                            //Do not show the sidebar on the shopping cart page and place your order
                            $postid = get_the_ID();
                            if (($postid == 7) || ($postid == 8) || ($postid == 9) || wp_is_mobile()) {
                        ?>
                        <div class="col-sm-12 col-md-12">
                        <?php
                            } else {
                        ?>
                        <div class="hidden-xs hidden-sm col-md-3">
                            <div class="content-sidebar-fixed"> 
                                <?php get_sidebar("shop-sidebar"); ?>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-9">
                        <?php 
                            }
                        ?>
                        
                            <?php
                            while ( have_posts() ) :
                                the_post();

                                get_template_part( 'template-parts/content', 'page' );

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;

                            endwhile; // End of the loop.
                            ?>
                        </div>
                    </div>
                </div>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
