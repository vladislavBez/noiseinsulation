$(document).ready(function () {

    //header search widget
    function initSearchWidget () {
        
        var isOpen = false;
        var $buttonSearch = $(".header-search");
        var $widgetSearch = $("#header-search-widget");
        var $searchIcon = $("#header-search-icon");
        
        function closeWidgetSearch() {
            $widgetSearch.css('top', -10);
            $searchIcon.removeClass("header-active-icon");
            isOpen = false;
        }
        
        function openWidgetSearch() {
            $widgetSearch.css('top', 90);
            $searchIcon.addClass("header-active-icon");
            isOpen = true;
        }
        
        $buttonSearch.click(function () {
            if (isOpen) {
                closeWidgetSearch();
            } else {
                openWidgetSearch();
            }
        });
        
        //Close widget when clicking outside the area
        $(document).mouseup(function (e){
            if (!$widgetSearch.is(e.target)
                && $widgetSearch.has(e.target).length === 0
                && isOpen 
                && !$buttonSearch.is(e.target) 
                && $buttonSearch.has(e.target).length === 0) { 
                closeWidgetSearch();
            }
        });
    }
    
    //header tablet menu
    function initTabletMenuWidget () {
        
        var isOpen = false;
        var $buttonTabletMenu = $("#mobile-icon-menu");
        var $widgetTabletMenu = $("#header-menu-widget");
        var $tabletMenuIcon = $("#header-tablet-menu-icon");
        var $tabletCloseIcon = $("#header-tablet-close-icon");
        var $fixedBackground = $("#mobile-fixed-background");
        var $body = $("#main-body");
        
        function closeWidgetTabletMenu() {
            $widgetTabletMenu.css('left', -485);
            $tabletMenuIcon.removeClass("hide");
            $tabletCloseIcon.addClass("hide");
            $fixedBackground.addClass("hide");
            $body.css('overflow', "auto");
            isOpen = false;
        }
        
        function openWidgetTabletMenu() {
            var clientHeight = document.documentElement.clientHeight;
            var widgetHeight = clientHeight - 50;
            $widgetTabletMenu.css('height', widgetHeight);
            $widgetTabletMenu.css('left', 0);
            $tabletMenuIcon.addClass("hide");
            $tabletCloseIcon.removeClass("hide");
            $fixedBackground.removeClass("hide");
            $body.css('overflow', "hidden");
            isOpen = true;
        }
        
        $buttonTabletMenu.click(function () {
            if (isOpen) {
                closeWidgetTabletMenu();
            } else {
                openWidgetTabletMenu();
            }
        });
        
        //Close widget when clicking outside the area
        $(document).mouseup(function (e){
            if (!$widgetTabletMenu.is(e.target)
                && $widgetTabletMenu.has(e.target).length === 0
                && isOpen 
                && !$buttonTabletMenu.is(e.target) 
                && $buttonTabletMenu.has(e.target).length === 0) { 
                closeWidgetTabletMenu();
            }
        });
    }
    
    //header tablet menu cart
    function initTabletMenuCartWidget () {

        var $desktopCartWidget = $(".wpmenucartli");

        var $tabletMenuCartCount = $("#tablet-cart-count");
        var $tabletMenuCartPrice = $("#tablet-cart-price");

        $desktopCartWidget.bind("DOMSubtreeModified", function () {

            var desktopCartWidgetCount = $(".wpmenucart-contents .cartcontents").text();
            var desktopCartWidgetPrice = $(".wpmenucart-contents .amount").text();

            $tabletMenuCartCount.text(desktopCartWidgetCount);
            $tabletMenuCartPrice.text(desktopCartWidgetPrice);
        });
    }

    function initFixedSidebarWidget () {
        $sidebar = $(".content-sidebar-fixed");
    }

    //back call header modal
    function initBackCallModal () {

        var $fixedBackground = $("#mobile-fixed-background");
        var $body = $("#main-body");
        var allButtonsOpenModalArray = $(".button-open-paper-modal");
        var $modal = $("#back-call-paper-modal");
        var $modalBody = $("#back-call-paper-modal .body-paper-modal");
        var $modalCloseButton = $("#back-call-paper-modal .close-paper-modal");

        function closeModal() {
            $modal.addClass("hide");
            $fixedBackground.addClass("hide");
            $body.css('overflow', "auto");
            isOpen = false;
        }
        
        function openModal() {
            $modal.removeClass("hide");
            $fixedBackground.removeClass("hide");
            $body.css('overflow', "hidden");
            isOpen = true;
        }

        //Open a modal window can have several buttons in different places
        allButtonsOpenModalArray.each(function (index, element) {

            var $button = $(element);

            $button.click(function () {
                openModal();
            });
        });

        $modalCloseButton.click(function () {
            closeModal();
        });
        
        //Close modal when clicking outside the area
        $(document).mouseup(function (e){
            if (!$modalBody.is(e.target)
                && $modalBody.has(e.target).length === 0) { 
                closeModal();
            }
        });
    }

    //header search widget
    initSearchWidget();
    //header tablet menu
    initTabletMenuWidget();
    //header tablet menu cart
    initTabletMenuCartWidget();
    //fixed sidebar
    initFixedSidebarWidget();
    //back call modals
    initBackCallModal();
});