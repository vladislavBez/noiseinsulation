$(document).ready(function () {
    
    var tileWidth;
    
    function setTileHeight() {
        tileWidth = $(".block-image-tile").innerWidth();
        $(".block-image-tile").css("height", tileWidth);
    }
    
    //When resizing the window, resize the tiles
    $(window).resize(function () {
        setTileHeight();
    });
    
    setTileHeight();
});