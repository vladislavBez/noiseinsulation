<?php
/**
 * Template Name: Blog
 *
 * @package NoiseInsulation
 */

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            
            <div class="container-content content-background-lite-gray">
                <div class="container">
                    <div class="row">
                        
                        <?php
                            //Do not show the sidebar on the shopping cart page and place your order
                            $postid = get_the_ID();
                            if (($postid == 7) || ($postid == 8) || ($postid == 9) || wp_is_mobile()) {
                        ?>
                        <div class="col-sm-12 col-md-12">
                        <?php
                            } else {
                        ?>
                            <div class="col-sm-3 col-md-3">
                            <?php 
                            get_sidebar(); 
                            ?>
                        </div>
                        <div class="col-sm-9 col-md-9">
                        <?php 
                            }
                        ?>
                            <header class="entry-header">
                                <h1 class="entry-title">Наш блог</h1>  
                            </header>

                            <div class="entry-content-list">
                                <article>     
                                    <?php
                                    $args = array(
                                        'cat' => 43,
                                        'paged' => get_query_var('paged'),
                                        'posts_per_page' => 10
                                    );
                                    query_posts($args);
                                    while (have_posts()) : the_post(); ?>
                             
                                    <div class="page-thumbnail-wrap">
                                        <?php noiseinsulation_post_thumbnail(); ?>
                                        <div class="page-header-description">
                                            <div>
                                                <div class="page-header-description-title">
                                                    <?php echo(get_post_meta($post->ID, 'Подзаголовок', true)); ?>
                                                </div>
                                                <div class="page-header-description-info">
                                                    <?php echo(get_post_meta($post->ID, 'Описание', true)); ?>
                                                </div>
                                            </div>
                                            <div class="page-header-description-button">
                                                <a href="<?php the_permalink(); ?>" class="btn-dark-red">Читать</a>
                                            </div>
                                        </div>
                                    </div>
                             
                                    <?php endwhile; ?>
                                </article>                            
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container-content content-background-lite-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                        <?php the_posts_pagination(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php wp_reset_query(); ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
