<?php
/**
 * Template Name: MainPage
 *
 * @package NoiseInsulation
 */

get_header();
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <!-- Hide title for seo -->
            <h1 class="hide">Шумоизоляция для автомобилей</h1>

            <div class="container-sliders">
                <div class="slider-paragraph-middle text-white slider-load-label">
                    Загрузка...
                </div>
                <div id="container-sliders-parallax">
                    <div id="main-slider">
                        <div class="main-slider-item">
                            <div class="main-slider-item-image main-slider-road-forest"></div>
                            <div class="main-slider-item-title">
                                <h2 class="slider-header text-white">Избавься от шума</h2>
                                <p class="slider-paragraph-middle text-white">Оставь только удовольствие от путешествия</p>
                                <button class="btn-dark-red btn-biggest button-open-paper-modal">Заказать звонок</button>
                            </div>
                            <div class="main-slider-item-bottom">
                                <div class="main-slider-item-bottom-title">
                                    <p class="slider-paragraph-middle text-white">Официальный дилер</p>
                                    <p class="slider-paragraph-middle text-white">в Ярославле</p>
                                </div>
                                <div class="main-slider-item-bottom-img-wrap">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/shumoff.png' ?>" class="main-slider-item-bottom-img">
                                </div>
                            </div>
                        </div>
                        <div class="main-slider-item">
                            <div class="main-slider-item-image main-slider-sound"></div>
                            <div class="main-slider-item-title">
                                <h2 class="slider-header text-white">Прокачай звук</h2>
                                <p class="slider-paragraph-middle text-white">Профессиональная шумоизоляция для профессионального звука</p>
                                <button class="btn-dark-red btn-biggest button-open-paper-modal">Заказать звонок</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="container-sliders-parallax-xs">
                    <div class="container-slider-mobile">
                        <div class="slider-mobile-block slider-mobile-block-titles">
                            <h2 class="slider-mobile-header text-white">Избавься от шума</h2>
                            <p class="slider-paragraph-mobile text-white">Оставь только удовольствие от путешествия</p>
                        </div>  
                        <div class="slider-mobile-block">
                            <div class="slider-mobile-substrate">
                                <div class="main-slider-item-bottom-title">
                                    <p class="slider-paragraph-middle text-white">Официальный дилер</p>
                                    <p class="slider-paragraph-middle text-white">в Ярославле</p>
                                </div>
                                <div class="main-slider-item-bottom-img-wrap">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/shumoff.png' ?>" class="main-slider-item-bottom-img">
                                </div>
                            </div>
                        </div>  
                        <div class="slider-mobile-block slider-mobile-block-button">
                            <a href="tel:+79301324075" class="btn-slider-mobile">
                                <button class="btn-dark-red btn-biggest">Позвонить</button>
                            </a>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="container-content content-background-lite-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <h2 class="content-header">Комплекты ШумOff</h2>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <a class="content-title content-title-right" href="<?php echo get_category_link(21); ?>">Посмотреть все комплекты</a>
                        </div>
                    </div>
                    <div class="row hidden-xs">
                        <div class="col-sm-12 col-md-12">
                            <?php echo do_shortcode('[products ids="252, 259, 250, 270" columns="4"]'); ?>
                        </div>
                    </div>
                    <div class="row visible-xs">
                        <div class="col-xs-12">
                            <div id="goods-in-sets" class="content-slider">
                                <div><?php echo do_shortcode('[product id="252"]'); ?></div>
                                <div><?php echo do_shortcode('[product id="259"]'); ?></div>
                                <div><?php echo do_shortcode('[product id="250"]'); ?></div>
                                <div><?php echo do_shortcode('[product id="270"]'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-content content-background-middle-lite-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <h2 class="content-header">Хиты ШумOff</h2>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <a class="content-title content-title-right" href="<?php echo get_category_link(21); ?>">Посмотреть все хиты</a>
                        </div>
                    </div>
                    <div class="row hidden-xs">
                        <div class="col-sm-12 col-md-12">
                            <?php echo do_shortcode('[products ids="84, 68, 53, 51" columns="4"]'); ?>
                        </div>
                    </div>
                    <div class="row visible-xs">
                        <div class="col-xs-12">
                            <div id="goods-in-actions" class="content-slider">
                                <div><?php echo do_shortcode('[product id="84"]'); ?></div>
                                <div><?php echo do_shortcode('[product id="68"]'); ?></div>
                                <div><?php echo do_shortcode('[product id="53"]'); ?></div>
                                <div><?php echo do_shortcode('[product id="51"]'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="request-call" class="container-request-call">
                <div id="container-request-call-parallax" class="hidden-xs">
                    <div class="container-slider-xs"></div>
                </div>
                <div class="container-content" id="container-parallax-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h2 class="content-header text-white">Установка</h2>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <a class="content-title content-title-right text-white"
                                   href="<?php echo get_permalink(117); ?>">Узнать больше</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <p class="content-paragraph-middle text-white align-left">Профессиональная установка шумоизоляции в фирменных установочных центрах</p>
                            </div>
                        </div>
                        <div class="row hidden-xs">
                            <div class="col-sm-12 col-md-12">
                                <div class="container-square-tiles margin-big-top">
                                    <a href="<?php echo get_permalink(117); ?>" class="square-tile block-image-tile col-tile-4 square-tile-lite">
                                        <div class="square-tile-icon">
                                            <img src="<?php echo get_template_directory_uri() . '/assets/images/quality-icon-white.png' ?>">
                                        </div>
                                        <p class="content-paragraph-middle text-white">Качество</p>
                                        <div class="square-tile-separator separator-white"></div>
                                        <p class="content-paragraph-small text-white">Гарантированное качество во всех видах работ</p>
                                    </a>
                                    <a href="<?php echo get_permalink(117); ?>" class="square-tile block-image-tile col-tile-4 square-tile-lite">
                                        <div class="square-tile-icon">
                                            <img src="<?php echo get_template_directory_uri() . '/assets/images/speed-icon-white.png' ?>">
                                        </div>
                                        <p class="content-paragraph-middle text-white">Скорость</p>
                                        <div class="square-tile-separator separator-white"></div>
                                        <p class="content-paragraph-small text-white">Точные сроки окончания работ</p>
                                    </a>
                                    <a href="<?php echo get_permalink(117); ?>" class="square-tile block-image-tile col-tile-4 square-tile-lite">
                                        <div class="square-tile-icon">
                                            <img src="<?php echo get_template_directory_uri() . '/assets/images/safe-icon-white.png' ?>">
                                        </div>
                                        <p class="content-paragraph-middle text-white">Ответственность</p>
                                        <div class="square-tile-separator separator-white"></div>
                                        <p class="content-paragraph-small text-white">Профессионализм и стандарты качества</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row visible-xs">
                            <div class="col-xs-12">
                                <a href="<?php echo get_permalink(117); ?>" class="square-mobile-container text-white square-tile-lite">
                                    <div class="square-mobile-image-wrap">
                                        <img class="square-mobile-image" src="<?php echo get_template_directory_uri() . '/assets/images/quality-icon-white.png' ?>">
                                    </div>
                                    <div class="square-mobile-separator">
                                    </div>
                                    <div class="square-mobile-paragraph">
                                        <h3>Качество</h3>
                                        <p>Гарантированное качество во всех видах работ</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row visible-xs">
                            <div class="col-xs-12">
                                <a href="<?php echo get_permalink(117); ?>" class="square-mobile-container text-white square-tile-lite">
                                    <div class="square-mobile-image-wrap">
                                        <img class="square-mobile-image" src="<?php echo get_template_directory_uri() . '/assets/images/speed-icon-white.png' ?>">
                                    </div>
                                    <div class="square-mobile-separator">
                                    </div>
                                    <div class="square-mobile-paragraph">
                                        <h3>Скорость</h3>
                                        <p>Точные сроки окончания работ</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row visible-xs">
                            <div class="col-xs-12">
                                <a href="<?php echo get_permalink(117); ?>" class="square-mobile-container text-white square-tile-lite">
                                    <div class="square-mobile-image-wrap">
                                        <img class="square-mobile-image" src="<?php echo get_template_directory_uri() . '/assets/images/safe-icon-white.png' ?>">
                                    </div>
                                    <div class="square-mobile-separator">
                                    </div>
                                    <div class="square-mobile-paragraph">
                                        <h3>Ответственность</h3>
                                        <p>Профессионализм и стандарты качества</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="container-content content-background-lite-gray">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <h2 class="content-header">Накопительные скидки</h2>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <a class="content-title content-title-right" href="<?php echo get_permalink(119); ?>">Узнать больше</a>
                </div>
            </div>
            <div class="row hidden-xs">
                <div class="col-sm-12 col-md-12">
                    <div class="container-square-tiles margin-big-top">
                        <a href="<?php echo get_category_link(42); ?>" class="square-tile block-image-tile col-tile-4 square-tile-dark">
                            <div class="square-tile-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/certificate-icon.png' ?>">
                            </div>
                            <p class="content-paragraph-middle">Подарочные сертификаты</p>
                            <div class="square-tile-separator separator-red"></div>
                            <p class="content-paragraph-small">Лучший подарок мужчине</p>
                        </a>
                        <a href="<?php echo get_permalink(119); ?>" class="square-tile block-image-tile col-tile-4 square-tile-dark">
                            <div class="square-tile-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/sale-icon.png' ?>">
                            </div>
                            <p class="content-paragraph-middle">Накопительные скидки</p>
                            <div class="square-tile-separator separator-red"></div>
                            <p class="content-paragraph-small">Конкурсы, акции и индивидуальные предложения</p>
                        </a>
                        <a href="<?php echo get_permalink(172); ?>" class="square-tile block-image-tile col-tile-4 square-tile-dark">
                            <div class="square-tile-icon">
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/chat-icon.png' ?>">
                            </div>
                            <p class="content-paragraph-middle">Сообщество</p>
                            <div class="square-tile-separator separator-red"></div>
                            <p class="content-paragraph-small">Авторский блог про установку шумоизоляции</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row visible-xs">
                <div class="col-xs-12">
                    <a href="<?php echo get_category_link(42); ?>" class="square-mobile-container-dark square-tile-dark">
                        <div class="square-mobile-image-wrap">
                            <img class="square-mobile-image" src="<?php echo get_template_directory_uri() . '/assets/images/certificate-icon.png' ?>">
                        </div>
                        <div class="square-mobile-separator-red">
                        </div>
                        <div class="square-mobile-paragraph">
                            <h3>Подарочные сертификаты</h3>
                            <p>Лучший подарок мужчине</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row visible-xs">
                <div class="col-xs-12">
                    <a href="<?php echo get_permalink(119); ?>" class="square-mobile-container-dark square-tile-dark">
                        <div class="square-mobile-image-wrap">
                            <img class="square-mobile-image" src="<?php echo get_template_directory_uri() . '/assets/images/sale-icon.png' ?>">
                        </div>
                        <div class="square-mobile-separator-red">
                        </div>
                        <div class="square-mobile-paragraph">
                            <h3>Накопительные скидки</h3>
                            <p>Конкурсы, акции и индивидуальные предложения</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row visible-xs">
                <div class="col-xs-12">
                    <a href="<?php echo get_permalink(172); ?>" class="square-mobile-container-dark square-tile-dark">
                        <div class="square-mobile-image-wrap">
                            <img class="square-mobile-image" src="<?php echo get_template_directory_uri() . '/assets/images/chat-icon.png' ?>">
                        </div>
                        <div class="square-mobile-separator-red">
                        </div>
                        <div class="square-mobile-paragraph">
                            <h3>Сообщество</h3>
                            <p>Авторский блог про установку шумоизоляции</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-content content-background-middle-gray visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <h2 class="content-header text-white">Наш адрес</h2>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <a class="content-title content-title-right text-white" href="<?php echo get_permalink(115); ?>">Посмотреть все контакты</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="map-contact-mobile">
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-map-marker"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <span>Ярославль, пр-кт Фрунзе, дом 3, офис 312</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-earphone"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <span>8 (4852) 68-40-75</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <span>info@accord76.shop</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <div>График работы:</div>
                                <div>Пн - Вт: 10:00 до 20:00</div>
                                <div>Сб - Вс: 10:00 до 15:00</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-map content-background-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 hidden-xs">
                    <div class="container-map-contact">
                        <div class="contact-map-title text-white">
                            Наши адреса
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-map-marker"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <span>Ярославль, пр-кт Фрунзе, дом 3, офис 312</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-earphone"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <span>8 (4852) 68-40-75</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <span>info@accord76.shop</span>
                            </div>
                        </div>
                        <div class="footer-menu-contact">
                            <div class="footer-contact-icon">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </div>
                            <div class="contact-map-text text-white">
                                <div>График работы:</div>
                                <div>Пн - Вт: 10:00 до 20:00</div>
                                <div>Сб: 10:00 до 15:00</div>
                                <div>Вс: выходной</div>
                            </div>
                        </div>
                        <div class="contact-map-all-contacts">
                            <a class="link-all-contacts text-white" href="<?php echo get_permalink(115); ?>">Посмотреть все контакты</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer-map">
           <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A96c7646d3f1394e63e6c7393bb68cadbb096e855b35d7a77cfc508fb4560aeb2&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
        </div>
    </div>

    </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
